<?php
/**
 * Created by Rubikin Team.
 * Date: 7/2/14
 * Time: 10:59 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * usage:
 * php week_1_day_1.php -ffilename.php
 * php week_1_day_1.php -x
 */

define ("CSV_FILE_PATH", __DIR__ . '/week1/day1/products.csv');
define ("CSV_FILE_EXPORT_PATH", __DIR__ . '/week1/day1/products_export.csv');
define ("DB_NAME", 'training');
define ("DB_USER", 'simple_shop');
define ("DB_PASS", 'simple_shop');
define ("TABLE_PRODUCT", 'product');


// DO NOT EDIT BELOW THIS LINE

$mysqli = new mysqli("localhost", DB_USER, DB_PASS, DB_NAME);

if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

/* turn autocommit on */
$mysqli->autocommit(TRUE);

$options = getopt("f::x::");

if (isset($options['x'])) {
    // reset table
    resetTable($mysqli);
    // reset file
    resetFile(CSV_FILE_EXPORT_PATH);

    exit('Reset database & file');
}


if (empty($options['f'])) {
    exit ("Missing filename");
}

if (!file_exists($file = __DIR__ . "/week1/day1/" . $options['f'])) {
    exit (sprintf("File %s does not exist", $file));
}


$now = time();
$memory = memory_get_usage();

// include the file to run
require($file);

echo "loading $file" . "\n";
echo "Total time: " . (time() - $now) . "\n";
echo "Total memory: " . memory_get_usage() - $memory . "\n";

$mysqli->close();

/**
 * The section below is dedicated for simple functions to re-use in next step of the project
 */
function resetTable(mysqli $mysqli)
{
    $mysqli->query("TRUNCATE TABLE " . TABLE_PRODUCT);
}

function resetFile($file)
{
    file_put_contents($file, '');
}